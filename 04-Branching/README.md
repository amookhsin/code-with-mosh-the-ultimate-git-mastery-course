# انشعابش

انشعاب یا branch در گیت تنها یه اشاره‌گر است که به یه برگرفت اشاره می‌کند. برای همین ایجاد انشعاب در گیت سریع است. هر بار که به انشعاب دیگر سوییچ می‌کنیم، گیت اشاره‌گر HEAD را روی آن انشعاب قرارداده و فضای‌کار را یکسان با برگرفت می‌کند. این رفتار مانند ایجاد چندین فضای‌کاری جداییده است.

## شروع کار با انشعاب

```shell
# Get all current branches
git branch

# To view the list of the merged branches
git branch --merged
# To view the list of the unmerged branches
git branch --no-merged

# Switch to a branch
git switch <branch_name>
# To create and switch to a branch at same time
git switch -C <branch_name>

# Rename a branch
git branch -m <old_name> <new_name>     # Convention: bugfix/detail, feature/detail

# Deleting a branch
git branch -d <branch_name>
git branch -D <branch_name> # Force the deletion

# Get all commits
git log --all
```

## مقایسیدن انشعاب‌ها

```shell
# Get all branch that there are in <second_branch> but not in <first_branch>
git log <first_branch>..<second_branch>     # Comparing with current branch: git log <second_branch>

# To see actual changes with compare <first_branch> with <second_branch>
git diff <first_branch>..<second_branch>    # Get only changed files: git diff --name-status <second_branch>
```

## انباریدن موقت فضای کار (Stashing)

یعنی برگرفتن از فضای‌کار در یه انشعاب جداییده:

```shell
# Stashing tracked files
git stash push -m "message" # Stashing untracked files, too: git stash push -ma "message"
# To view our stashes
git stash list
# To lock at changes
git stash show stash@{<index>}  # == git stash show <index>
# To apply the changes on the working directory:
git stash apply stash@{<index>}
# Removing a stash
git stash drop <index>  # Remove all stash: git stash clear
```

## ادغامیدن

انوع ادغام:

- Fast-forward: هنگامیکه دو انشعاب ناواگرا (بر یک خط) باشند:
  ![ادغام fast-forward](assets/fast-forward-merge.png)
- 3-way: هنگامیکه دو انشعاب واگرا باشند:
  ![ادغام three-way](assets/three-way-merge.png)

مقایسه دو نوع ادغام:

- در ادغام fast-forward تاریخچه تغییرها نگهداشته می‌وشد.
- در ادغام three-way تاریخچه خلوت‌تر می‌ماند.

### اجباریدن گیت در انتخاب نوع ادغام

به‌شکل زیر می‌توان نوع ادغامیدن را بدون توجه به تشخیص گیت تعینید:

```shell
# To force git to use 3-way merging
git merge --no-ff <target_branch>
# To show all branches graphically
git log --all --oneline --graph

# To disable fast-forward merging on current repository
git config merge.ff no
# To disable fast-forward merging on all repositories
git config --global merge.ff no
```

## رفعیدن تضادها (conflicts)

فرایند ادغامیدن و برخورد با تضاد:

1. اگر گیت هنگام ادغامیدن به تضاد برخورد در وضیت رفع تضاد قرار می‌گید؛
2. که باید تضادها را به‌شکل دستی یا با ابزاری رفعید؛
3. سپس بادستور `git commit` دوباره انشعاب‌ها را ادغامید.

**توجه.** برای خروج از وضعیت رفعیدن تضاد از دستور `git merge --abort` می‌استفیم.

### ابزار p4merge

پیکریدن گیت برای استفاده از p4merge:

```shell
git config --global merge.tool p4merge
git config --global mergetool.p4merge.path "<path_to_p4merge>"
# To prevent the mergetool from automatically generate a backup files
git config --global mergetool.keepBackup false

# To use
git mergetool

# To abort the merging
git merge --abort
```

## بیاثریدن ادغام نادرست

دو ریکرد وجود دارد:

1. حذفیدن کامیت: تنها اشاره‌گرهای انشعاب و HEAD تغییر می‌کنند.

   **هشدار.** این روشکرد، به‌دلیل تغییر در تاریخچه، هنگام کار با مخزن (repository) برخط (online) توصیه نمی‌شود

   <div dir="ltr" align="left" markdown="1">

   ```shell
   # Option1. Without affecting on staging area and work directory
   git reset --soft <commit_id>
   # Option2. Take that snapshot and put it in the staging area
   git reset --mixed <commit_id> # by default
   # Option3. Take that snapshot and puting it in the work directory
   git reset --hard <commit_id>
   ```

   </div>

   **نکته.** بازنشانی یه کامیت باعث حذف آن از مخزن نمی‌شود، بلکه به نوعی عقیم می‌شود.به‌عبارتی با داشتن شناسه یه کامیت حتی پس از بازنشانی بازهم می‌توان ازش استفید.

2. بازآوردن کامیت: این باعث ایجاد یه کامیت جدید با برگفت‌های پیشین می‌شود.

   <div dir="ltr" align="left" markdown="1">

   ```shell
   # To revert a commit
   git revert <commit_id>

   # To revert a range of commits one by one
   git revert HEAD~2..HEAD

   # To add required changes in staging area
   git revert --no-commit HEAD~3.. # === HEAD~3..HEAD
   # To continue to merging
   git revert --continue

   # -m: select mainline parent
   git revert -m 1 <commit_id>
   ```

   </div>

## آوردن تغییرها در یه انشعاب

روش‌های گوناگونی برای تکشاخه ماندن یه انشعاب وجود دارد؛ که بنیا به خواسته یکی از آن‌ها سودمند است.

### ادغامیدن بدون وابستگی به دو انشعاب (نگهنداری تاریخچه)

در این روش تنها تغییرهای انشعاب دیگر در staging area قرار می‌گیرد. این روش برای تمیز نگهداشتن تاریخچه بسیار سودمند است.

![ادغام squash](assets/merge-squash.png)

**دستورها:**

```shell
# To push all changes from <branch_name> to staging area
git commit --squash=<branch_name>
# To commit
git commit
```

### دگرشالودن (Rebasing)

در این روش با تغییر شالوده یه انشعاب، همه‌ی تغییرهای آن انشعاب بر شالوده‌ی جدید نهاده می‌شود.

![دگرشالودن](assets/rebasing.png)

**توجه.** این روش به دلیل تغیر تاریخچه تنها برای مخزن‌های محلی توصیه می‌شود.

**دستورها:**

```shell
# On target branch
git rebase <branch_name>

# If the rebasing end up with conflict
# To solve the conflict (in the middle of rabasing)
git mergetool
# OR, to continue rebase (after conflict resolved)
git rebase --continue
# OR, to skip current patch and continue
git rebase --skip
# OR, to abort and check out the original branch
git rebase --abort
```

## ادغامیدن با یه کامیت خاص

برای ادغامیدن یه انشعاب با یه کامیت خاص از انشعاب دیگر یا آوردن تغییرهای آن کامیت در staging area.

![گیت cherry-pick](assets/git-cherry-pick.png)

```shell
# To commit with a specific commit
git cherry-pick <commit_id>

# To bring the changes in staging area
git cherry-pick -n <commit_id>
```

## آوردن یه فایل خاص از یه کامیت

آوردن یه فایل خاص از یه کامیت در staging area:

```shell
git restore --source=<commit_id> -- <path_to_file>
```
