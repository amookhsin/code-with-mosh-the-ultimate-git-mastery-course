# مرور تاریخچه

## دیدن تاریخچه

```shell
git log
# A summary of all commits
git log --oneline
# See all files that have been changed in each commit
git log --stat
# See actual changes for each commit
git log --patch
```

### گزیدن از تاریخچه

```shell
git log --author="<auth_name>"
# Befor OR After a date
git log --before="yesterday"
git log --after="2020-08-17"
# Filter by commit message
git log --grep="<message>"
# Filter by content
git log -S "<content>"
# Filter by a rage of commits
git log <commit_id_1>..<commit_id_2>
# Filter all of commit that touch a file
git log <file_name>
```

### سفاریدن log

```shell
# Doc: https://git-scm.com/docs/git-log
git log --pretty=format:"%Cgreen%an%Creset commited %h on %cd"
```

## تعریف alians

```shell
git congig --global alias.<command_name> "git command"
```

## نمودن یه commit

```shell
git show HEAD~2 --name-only
git show HEAD~2 --name-status
```

## نمودن تغییرها در چند commit

```shell
git diff HEAD~2 HEAD
git diff HEAD~2 HEAD --name-only
git diff HEAD~2 HEAD --name-status
git diff HEAD~2 HEAD <path_to_file>
```

## حرکت اشاره‌گر HEAD

```shell
git checkout <commit_id>
```

## یافتن باگ به‌روش تقسیم میانه

```shell
git bisect star                 # To start to find bad commit
git bisect bad                  # To define that curent commit is bad
git bisect good <commit_id>     # To define the good commit
git bisect reset                # To atach HEAD pointer to the master branch
```

## یافتن همه‌ی مشارک‌کننده‌ها و فعالیت‌هایشان

```shell
git shortlog
git shortlog -n     # sort according to the number of commit per author
git shortlog -s     # suppress commit descriptions
```

## برگرداندن فایل‌های حذفیده

```shell
git checkout <commit_id> <path_to_file>
```

## یافتن تغییرنده‌ی خط‌های یه فایل

```shell
git blame -L <start_line>,<end_line> <path_to_file> # Ex: git blame -L 1,3 exm.txt
```

## تگیدن یه کامیت

```shell
git tag <tag_name> <commit_id>
# To define a annotated tag
git tag -a <tag_name> -m "<message>"
# To list all tag
git tag
# To list all tag with its messages
git tag -n
```
