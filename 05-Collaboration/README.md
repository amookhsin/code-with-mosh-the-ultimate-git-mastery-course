# همکنشیدن (collaboration)

برای همکنشیدن در توسعه یه نرم‌افزار با استفاده از گیت، که از نوع توزیعیده است، از یه مخزن مرکزی می‌استفیم. جریان کار پروژه‌های خصوصی اینه که هرکس از مخزن مرکزی یه کلون می‌گیرد. سپس توسعه را در مخزن محلی خودش انجام می‌دهد، و درنهایت روی مخزن مرکزی push می‌کند. دیگر توسعه‌دهنده‌ها هم برای همگام‌شدن، تغییرها را از مخزن مرکزی pull می‌کنند. مخزن مرکزی می‌توان یه سرور اختصاصی یا یه میزبان گیت (git hosting) باشد.

![روندکار مرکزسازیده](assets/centralized_workflow.png)

روندکار برای پروژه‌های منبع‌باز نیز اینجور که هرکس از مخزن یه fork می‌گیرد، و پس از توسعه یه درخواست ادغام به صاحب مخزن اصلی می‌فرستد.

![روندکار پروژه‌های منبع‌باز](assets/integration_manager.png)

## یکسانزادن (cloning)

برای یکسانزادن از یه مخزن از دستور زیر، در مسیر موردنظر، می‌استفیم:

```shell
git clone <url_of_repository>
# OR
git clone <url_of_repository> <ProjectName>

# To show all remote repository
git remote
# To show all remote with more detail
git remote -v
```

**توجه.** انشعاب‌های `origin/master` و `origin/HEAD` مربوط به مخزن دور هستند و عملکردشان همانند انشعاب‌های معمولی نیست؛ یعنی نمی‌توان دستور همانند `git checkout origin/master` را اجرایید. این انشعاب‌ها remote tracking branch نامیده می‌شوند.

## همگامیدن مخزن محلی

به چند روش می‌توان مخزن محلی را با مخزن دور همگامید.

### رفتاوردن

در این روش تغییرها در مخزن محلی آورده می‌شود ولی فضای‌کار هیچ تغییری نمی‌کند. برای اعمال تغییرها باید به ادغامیدن اقدامید.

![رفتاوردن](assets/git_fetch.png)

```shell
git fetch <remote>
# OR, to fetch a specific branch
git fetch <remote> <remote_branch>

# To show how local and remote branch are diverging
git branch -vv

# To merging when we are on target branch execute
git merge <remote/remote_branch>
```

**توجه.** رفتاوردن انشعاب‌های دور باعث ایجاد انشعاب محلی نمی‌شود؛ باید به‌شکل زیر آن‌ها را به یه انشعاب محلی نگاشت:

```shell
# To map a remote tracking branch to a local
git switch -C <branch_name> <remote_tracking_branch_name>

# To show remote branches
git branch -r
```

### رفتاوردن و ادغامیدن در یک گام

در این روش پس از رفتاوردن، تغییرها به‌شکل خودکار با انشعاب محلی نیز ادغامیده می‌شوند.

![رفتاوردن و ادغامیدن در یک گام](assets/git_pull.png)

```shell
git pull

# To rebase instead of merging
git pull --rebase
```

### هرسیدن انشعاب‌های دور

برای حذفیدن انشعاب‌های دور که دیگر وجود ندارند، از دستور زیر می‌استفیم:

```shell
git remote prune <remote>
```

## همگامیدن مخزن دور

برای همگامیدن مخزن دور با مخزن محلی‌امان از دستور `push` می‌استفیم.

![همگامیدن مخزن دور](assets/git_push.png)

```shell
git push <remote> <local_branch>

# To drop the deferent commits (NO RECOMMENDED)
git push -f <remote> <local_branch>

# To store git credentials permanently
git config --global credential.helper store

# To remove a remote branch
git push -d <remote> <branch_name>
```

### همگامیدن تگ‌ها

برای همگامیدن تگ‌های محلی با دور از دستور زیر می‌استفیم:

```shell
# To tag current commit
git tag <tag_name>
# To tag an specific commit
git tag <tag_name> <commit_id>

# To remove tag from local repository
git tag -d <tag_name>

# To push the tag
git push origin <tag_name>

# To remove a tag from remote repository
git push origin --delete <tag_name>
```
