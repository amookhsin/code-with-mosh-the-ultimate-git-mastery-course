# ساخت برگرفت

**نکته:** برخلاف ورژنکنترل‌های دیگر گیت از اختلاف‌ها برگرفت نمی‌گیرد، بلکه از کل فایل‌ها یه برگرفت می‌تهید.

**توصیه** بهتره تغییرهای مرتبط را باهم کامیت بگیریم.

## دستورهای پرکاربرد

```shell
# Initializing
git init

# Look at status
git status
git status -s # look at status in short mod

# Staging Files
git add <file1> <file2>
git add *.txt
git add .

# Unstaging Files
git restore --staged <path_to_file> # *.js .

# Commit the changes
git commit
git commit -m "commit message"
git commit -a -m "commit message" # add files into stage area and commit those in one go

# Discarding Local Changes
git restore <path_to_file> # . *.js
git clean -fd   # To remove all untracking files

# Restoring a file from storage
git restore --source=HEAD~1 <path_to_file>

# The files that are traking
git ls-files

# Get all files and directories that stored on a commit
git ls-tree <commit_id> # HEAD

# Removing
git rm <file_name> # removing files from staging area and work directory in one go
git rm --cached -r <path> # removint files only from staging area

# Moving
git mv <origin_path> <dist_path> # moving a file in both work directory and staging area

# Log
git log
git log --oneline --reverse

# View an object in git database
git show <commit_id> # HEAD~1
git show HEAD~1:<path_to_file> # too see the final version that is stored in the commit
```

## چشمپوشش گیت `.gitignore`

با ساخت این فایل در ریشه‌ی پروژه می‌توان فایل‌هایی که گیت باید چشمپوشد را درونش تعریفید.

**توجه:** گیت از فایل‌های کامیتیده در مخزن هرگز نمی‌چشمپوشد!

## سنجیدن تفاوت‌ها

پیکریدن گیت برای تعریف ابزار مقایسه‌ی دیداری:

```shell
git config --global diff.tool vscode
git config --global difftool.vscode.cmd "code --wait --diff $LOCAL $REMOTE"
```
