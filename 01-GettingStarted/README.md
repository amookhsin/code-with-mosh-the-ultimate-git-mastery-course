# شروع کار با گیت

دو معماری ورژن کنترلر وجود دارد:

1. مرکزیده: منبع کد در یه مرکز جا قرارگرفته و هر توسعه‌دهنده مستقیم بر آن اثر می‌گذارد.

   ![معماری مرکزیده](assets/centralized_architecture.png)

2. توزیعیده: هر توسعه‌دهنده در مخزن محلی‌اش یه کپی از منبع پروژه دارد.

   ![معماری توزیعیده](assets/distributed_architecture.png)

## سطح‌های پیکربندی

در گیت سه سطح پیکربندی وجود دارد:

1. سطح سیستم: اثرگذاری بر کل سیستم‌عامل جاری
2. سطح سراسری: اثرگذار بر کاربر جاری
3. سطح ریپازیتوری: اثر بر ریپازیتوری جاری

## پیکربندی بایسته

حداقل پیکربندی برای شروع کار با گیت:

```shell
git config --global user.name "User Name"
git congig --global user.emal user_email@emil.com
git congig --global core.editor "code --wait"
git config --global core.autocrlf input # for window -> true
```
